import { Injectable } from '@angular/core';


import {Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {Headers} from '@angular/http';
import {Comic} from '../../managing/shared/comic';
import { URLSearchParams } from '@angular/http';

@Injectable()
export class OrderComicService {
  private comicsUrl = 'http://localhost:8000/comics';  // URL to web api
  private orderUrl = 'http://localhost:8000/order';  // URL to web api

  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }

  getComics(): Promise<Comic[]> {
    const params = new URLSearchParams();
    params.set('display', 'on');
    return this.http.get(this.comicsUrl, {search: params})
      .toPromise()
      .then(response => response.json() as Comic[])
      .catch(this.handleError);
  }
  getComic(id: number): Promise<Comic> {
    const url = `${this.comicsUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Comic)
      .catch(this.handleError);
  }
  orderComic(id: number, email: string): Promise<any> {
    const data = {
      id: id,
      email: email
    };
    return this.http
      .post(this.orderUrl, JSON.stringify(data), {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

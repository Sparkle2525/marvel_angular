import {Component, Input, OnInit, OnDestroy, ViewChild, ElementRef} from '@angular/core';

import {ActivatedRoute, ParamMap} from '@angular/router';
import {OrderComicService} from '../shared/comic-order.service';
import {Comic} from '../../managing/shared/comic';


@Component({
  selector: 'app-comic-list',
  templateUrl: './comic-list.component.html',
  styleUrls: ['../../searching/comic-search/comic-search.component.css', './comic-list.component.css']
})
export class ComicListComponent implements OnDestroy, OnInit {
  comics: Comic[];
  searchDisable: boolean;
  busy: Promise<any>;

  innerHeight: any;
  innerWidth: any;

  constructor(private orderComicService: OrderComicService,
              private route: ActivatedRoute) {
    this.innerHeight = (window.screen.height - 150) + 'px';
    this.innerWidth = (window.screen.width) + 'px';
  }
  ngOnDestroy(): void {
  }
  ngOnInit(): void {
    this.busy = this.orderComicService.getComics().then(comics => {
      this.comics = comics;
    });
  }
}

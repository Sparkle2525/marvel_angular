import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ComicListComponent} from './comic-list/comic-list.component';
import {ComicDetailComponent} from './comic-detail/comic-detail.component';

const comicDetail: Routes = [
  { path: ':id',  component: ComicDetailComponent }
];

const routes: Routes = [
  { path: 'comics',  component: ComicListComponent, children: comicDetail }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class OrderRoutingModule {}

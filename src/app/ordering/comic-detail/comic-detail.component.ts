import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import {Subscription} from 'rxjs/Subscription';
import {Comic} from '../../managing/shared/comic';
import {OrderComicService} from '../shared/comic-order.service';


@Component({
  selector: 'app-comic-detail',
  templateUrl: './comic-detail.component.html',
  styleUrls: ['../../searching/comic-detail/comic-detail.component.css',
     './comic-detail.component.css',
    '../../shared/simpleGal.css']
})
export class ComicDetailComponent implements OnInit, OnDestroy {
  constructor(
    private orderComicService: OrderComicService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) {}
  private subscription: Subscription;
  displayOrderForm = true;
  previewImage: any;
  comic: Comic;
  comicToChange: Comic;
  hideDetails: {val: boolean};
  busy: Promise<any>;
  ngOnInit(): void {
    this.hideDetails = {val: false};
    this.subscription = this.route.paramMap
      .switchMap((params: ParamMap): Promise<Comic> => {
        const comicPromise = this.orderComicService.getComic(+params.get('id'));
        this.busy = comicPromise;
        return comicPromise;
      }).subscribe(comic => {
        this.comic = comic;
        this.previewImage = comic.images[0] || {path: comic.thumbnail.path, extension: comic.thumbnail.extension};
      });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  order(email): void {
    this.busy = this.orderComicService.orderComic(this.comic.id, email).then(() => {
      this.displayOrderForm = false;
    });
  }
  goBack(): void {
    this.location.back();
  }
}

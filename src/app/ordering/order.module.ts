import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// Imports for loading & configuring the in-memory web api


import {HttpModule} from '@angular/http';

import {MomentModule} from 'angular2-moment';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BusyModule} from 'angular2-busy';
import {ComicListComponent} from './comic-list/comic-list.component';
import {OrderComicService} from './shared/comic-order.service';
import {OrderRoutingModule} from './order-routing.module';
import {ComicDetailComponent} from './comic-detail/comic-detail.component';


@NgModule({
  imports: [
    FormsModule,
    MomentModule,
    HttpModule,
    BrowserAnimationsModule,
    BusyModule,
    OrderRoutingModule
  ],
  declarations: [
    ComicListComponent,
    ComicDetailComponent
  ],
  providers: [
    OrderComicService
  ]
})
export class OrderModule {
}

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// Imports for loading & configuring the in-memory web api


import {HttpModule} from '@angular/http';

import {MomentModule} from 'angular2-moment';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BusyModule} from 'angular2-busy';
import {ComicListComponent} from './comic-list/comic-list.component';
import {ComicDetailComponent} from './comic-detail/comic-detail.component';
import {ManageComicService} from './shared/comic-manage.service';
import {ManageRoutingModule} from './manage-routing.module';
import {ComicChangeComponent} from './comic-detail/comic-change/comic-change.component';
import { NguiDatetimePickerModule } from '@ngui/datetime-picker';


@NgModule({
  imports: [
    FormsModule,
    MomentModule,
    HttpModule,
    BrowserAnimationsModule,
    BusyModule,
    ManageRoutingModule,
    NguiDatetimePickerModule
  ],
  declarations: [
    ComicListComponent,
    ComicDetailComponent,
    ComicChangeComponent
  ],
  providers: [
    ManageComicService
  ]
})
export class ManageModule {
}

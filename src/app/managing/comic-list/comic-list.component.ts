import {Component, Input, OnInit, OnDestroy, ViewChild, ElementRef} from '@angular/core';
import {Comic} from '../shared/comic';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';
import {ManageComicService} from '../shared/comic-manage.service';


@Component({
  selector: 'app-comic-list',
  templateUrl: './comic-list.component.html',
  styleUrls: ['./comic-list.component.css', '../../searching/comic-search/comic-search.component.css']
})
export class ComicListComponent implements OnDestroy, OnInit {
  comics: Comic[];
  searchDisable: boolean;
  busy: Promise<any>;

  innerHeight: any;
  innerWidth: any;

  constructor(private manageComicService: ManageComicService,
              private route: ActivatedRoute) {
    this.innerHeight = (window.screen.height - 225) + 'px';
    this.innerWidth = (window.screen.width) + 'px';
  }
  ngOnDestroy(): void {
    // this.subscription.unsubscribe();
  }
  ngOnInit(): void {
    this.busy = this.manageComicService.getComics().then(comics => {
      this.comics = comics;
    });
  }
  refresh(): void {
    this.ngOnInit();
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ComicDetailComponent} from './comic-detail/comic-detail.component';
import {ComicListComponent} from './comic-list/comic-list.component';


const comicDetail: Routes = [
  { path: ':id', component: ComicDetailComponent }
];


const routes: Routes = [
  { path: 'master',  component: ComicListComponent, children: comicDetail }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class ManageRoutingModule {}

import DateTimeFormat = Intl.DateTimeFormat;

export class Comic {
  id: number;
  description: string;
  title: string;
  date: DateTimeFormat;
  ean: string;
  variants: Variant[];
  images: Image[];
  characters: Character[];
  stories: Story[];
  thumbnail: Thumbnail;
  active: boolean;
}

export class Thumbnail {
  path: string;
  extension: string;
}

export class Variant {
  id: number;
  name: string;
}

export class Image {
  id: number;
  path: string;
  extension: string;
}

export class Character {
  id: number;
  name: string;
  role: string;
}

export class Story {
  id: number;
  name: string;
  type: string;
}

export class ToRemoveContainer {
  variantsToRemove: Array<number>;
  charactersToRemove: Array<number>;
  storiesToRemove: Array<number>;
  constructor() {
    this.variantsToRemove = [];
    this.charactersToRemove = [];
    this.storiesToRemove = [];
  }
  serveVariant(variant, checked): void {
    if (checked) {
      this.variantsToRemove.push(variant.id);
    } else {
      this.variantsToRemove.splice(this.variantsToRemove.indexOf(variant.id), 1);
    }
  }
  serveStories(story, checked): void {
    if (checked) {
      this.storiesToRemove.push(story.id);
    } else {
      this.storiesToRemove.splice(this.storiesToRemove.indexOf(story.id), 1);
    }
  }
  serveCharacters(character, checked): void {
    if (checked) {
      this.charactersToRemove.push(character.id);
    } else {
      this.charactersToRemove.splice(this.charactersToRemove.indexOf(character.id), 1);
    }
  }
}

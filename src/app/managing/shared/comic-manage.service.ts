import { Injectable } from '@angular/core';

import { Comic } from './comic';
import {Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {Headers} from '@angular/http';

@Injectable()
export class ManageComicService {
  private comicsUrl = 'http://localhost:8000/comics';  // URL to web api

  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) { }

  getComics(): Promise<Comic[]> {
    return this.http.get(this.comicsUrl)
      .toPromise()
      .then(response => response.json() as Comic[])
      .catch(this.handleError);
  }
  getComic(id: number): Promise<Comic> {
    const url = `${this.comicsUrl}/${id}`;
    return this.http.get(url)
      .toPromise()
      .then(response => response.json() as Comic)
      .catch(this.handleError);
  }
  update(comic: any): Promise<Comic> {
    comic.date.setHours(comic.date.getHours() - comic.date.getTimezoneOffset() / 60);
    const url = `${this.comicsUrl}/${comic.id}`;
    return this.http
      .put(url, JSON.stringify(comic), {headers: this.headers})
      .toPromise()
      .then(response => response.json() as Comic)
      .catch(this.handleError);
  }
  deleteComic(id: number): Promise<void> {
    const url = `${this.comicsUrl}/${id}`;
    return this.http.delete(url, {headers: this.headers})
      .toPromise()
      .then(() => null)
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

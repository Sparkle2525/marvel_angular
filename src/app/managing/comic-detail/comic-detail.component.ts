import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import {Comic} from '../shared/comic';
import {Subscription} from 'rxjs/Subscription';
import {ManageComicService} from '../shared/comic-manage.service';

@Component({
  selector: 'app-comic-detail',
  templateUrl: './comic-detail.component.html',
  styleUrls: ['../../searching/comic-detail/comic-detail.component.css', './comic-detail.component.css', '../../shared/simpleGal.css'],

})
export class ComicDetailComponent implements OnInit, OnDestroy {
  constructor(
    private manageComicService: ManageComicService,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router
  ) {} // каждый раз создается новый экземпляр
  private subscription: Subscription;
  previewImage: any;
  comic: Comic;
  comicToChange: Comic;
  hideDetails: {val: boolean};
  busy: Promise<any>;
  ngOnInit(): void {
    this.hideDetails = {val: false};
    this.subscription = this.route.paramMap
      .switchMap((params: ParamMap): Promise<Comic> => {
          const comicPromise = this.manageComicService.getComic(+params.get('id'));
          this.busy = comicPromise;
          return comicPromise;
        }).subscribe(comic => {
          this.comic = comic;
          this.previewImage = comic.images[0] || {path: comic.thumbnail.path, extension: comic.thumbnail.extension};
      });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  goBack(): void {
    this.location.back();
  }
  deleteComic(comic): void {
    this.busy = this.manageComicService.deleteComic(comic.id).then(comics => {
      this.goBack();
    });
  }
  changeComic(): void {
    this.hideDetails.val = true;
    this.comicToChange = Object.assign({}, this.comic);
  }
}

import { Component, Input } from '@angular/core';
import {Comic, ToRemoveContainer} from '../../shared/comic';
import {ManageComicService} from '../../shared/comic-manage.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-comic-change',
  templateUrl: './comic-change.component.html',
  styleUrls: ['../../../searching/comic-detail/comic-detail.component.css',
    './comic-change.component.css', '../comic-detail.component.css']
})
export class ComicChangeComponent {
  @Input() refreshComic: any;
  @Input() comic: Comic;
  @Input() visibility: any;
  busy: Promise<any>;
  toRemove: ToRemoveContainer = new ToRemoveContainer();
  constructor(private manageComicService: ManageComicService,
              private location: Location ) {}
  cancel(): void {
    this.toRemove = new ToRemoveContainer();
    this.visibility.val = false;
  }
  save(): void {
    this.busy = this.manageComicService.update(Object.assign(this.comic, this.toRemove)).then(comic => {
      this.location.back();
    });
  }
  addToRemove(obj, type, event): void {
    if (type === 'story') {
      this.toRemove.serveStories(obj, event.target.checked);
    }
    if (type === 'character') {
      this.toRemove.serveCharacters(obj, event.target.checked);
    }
    if (type === 'variant') {
      this.toRemove.serveVariant(obj, event.target.checked);
    }
  }
}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {AppComponent} from './app.component';

// Imports for loading & configuring the in-memory web api

import {AppRoutingModule} from './app-routing.module';

import {PageNotFoundComponent} from './not-found/not-found.component';
import {SearchModule} from './searching/search.module';
import {ManageModule} from './managing/manage.module';
import {OrderModule} from './ordering/order.module';

@NgModule({
  imports: [
    BrowserModule,
    SearchModule,
    ManageModule,
    OrderModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent,
    PageNotFoundComponent
  ],
  providers: [
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}

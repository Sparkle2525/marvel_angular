import DateTimeFormat = Intl.DateTimeFormat;

export class Comic {
  id: number;
  description: string;
  title: string;
  dates: Date[];
  ean: string;
  variants: ComicSummary[];
  images: Image[];
  characters: CharacterList;
  stories: StoryList;
  thumbnail: Thumbnail;
}

class Thumbnail {
  extension: string;
  path: string;
}

class ComicSummary {
  resourceURI: string;
  name: string;
}

class Date {
  type: string;
  date: DateTimeFormat;
}

export class Image {
  path: string;
  extension: string;
}

class CharacterList {
  available: number;
  returned: number;
  collectionURI: string;
  items: CharacterSummary[];
}

class CharacterSummary {
  resourceURI: string;
  name: string;
  role: string;
}

class StoryList {
  available: number;
  returned: number;
  collectionURI: string;
  items: StorySummary[];
}

class StorySummary {
  resourceURI: string;
  name: string;
  type: string;
}

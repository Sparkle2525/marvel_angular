import { Injectable } from '@angular/core';

import { Comic } from './comic';
import {Http} from '@angular/http';
import 'rxjs/add/operator/toPromise';
import {Headers} from '@angular/http';

import { URLSearchParams } from '@angular/http';
import {Md5} from 'ts-md5/dist/md5';
import has = Reflect.has;

@Injectable()
export class SearchComicService {
  public lastSearchedTitle: string;
  private privateKey = '1e4829f1293c1f874bf4657f66b035159630abbb';
  private publicKey = '755f3876569e7bee30bb6f85b3ffc692';
  private comicsUrl = 'https://gateway.marvel.com:443/v1/public/comics';
  private saveUrl = 'http://localhost:8000/comics';
  private ifExistUrl = 'http://localhost:8000/if_exist';

  private headers = new Headers({'Content-Type': 'application/json'});

  constructor(private http: Http) {}

  getComics(title): Promise<Comic[]> {
    const timestamp = (+ new Date()) + '';
    const params = new URLSearchParams();
    const hash = Md5.hashStr(timestamp + this.privateKey + this.publicKey) + '';
    params.set('ts', timestamp);
    params.set('hash', hash);
    params.set('apikey', this.publicKey);
    params.set('format', 'comic');
    params.set('formatType', 'comic');
    params.set('title', title);
    return this.http.get(this.comicsUrl, {search: params})
      .toPromise()
      .then(response => response.json().data.results as Comic[])
      .catch(this.handleError);
  }
  getComic(id: number): Promise<Comic> {
    const timestamp = (+ new Date()) + '';
    const params = new URLSearchParams();
    const hash = Md5.hashStr(timestamp + this.privateKey + this.publicKey) + '';
    params.set('ts', timestamp);
    params.set('hash', hash);
    params.set('apikey', this.publicKey);
    const url = `${this.comicsUrl}/${id}`;
    return this.http.get(url, {search: params})
      .toPromise()
      .then(response => {
        console.log(response.json());
        return response.json().data.results[0] as Comic;
      })
      .catch(this.handleError);
  }
  add(comic: Comic): Promise<Comic> {
    const data = {
      description: comic.description,
      title: comic.title,
      date: comic.dates[0].date,
      ean: comic.ean,
      variants: comic.variants,
      images: comic.images,
      characters: comic.characters.items,
      stories: comic.stories.items,
      thumbnail: comic.thumbnail
    };
    return this.http
      .post(this.saveUrl, JSON.stringify(data), {headers: this.headers})
      .toPromise()
      .then(res => res.json() as Comic)
      .catch(this.handleError);
  }
  ifComicExists(title: string): Promise<any> {
    const params = new URLSearchParams();
    params.set('title', title);
    return this.http.get(this.ifExistUrl, {search: params})
      .toPromise()
      .then(response => {
        return response.json();
      })
      .catch(this.handleError);
  }
  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}

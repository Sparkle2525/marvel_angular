import {Component, Input, OnInit, OnDestroy, ViewChild, ElementRef} from '@angular/core';
import {Comic} from '../shared/comic';
import {SearchComicService} from '../shared/comic.service';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';


@Component({
  selector: 'app-comic-search',
  templateUrl: './comic-search.component.html',
  styleUrls: ['./comic-search.component.css']
})
export class ComicsSearchComponent implements OnDestroy, OnInit {
  comics: Comic[];
  searchDisable: boolean;
  busy: Promise<any>;
  @ViewChild('searchResult') private searchContainer: ElementRef;
  @ViewChild('searchInput') private searchInput: ElementRef;

  innerHeight: any;
  innerWidth: any;

  constructor(private searchComicService: SearchComicService,
              private route: ActivatedRoute) {
    this.innerHeight = (window.screen.height - 270) + 'px';
    this.innerWidth = (window.screen.width) + 'px';
  }

  search(title): void {
    if (!title) {
      return;
    }
    this.searchComicService.lastSearchedTitle = title;
    this.busy = this.searchComicService.getComics(title).then(comics => {
      this.comics = comics;
      this.searchContainer.nativeElement.scrollTop = 0;
    });
  }
  ngOnDestroy(): void {
  }
  ngOnInit(): void {
    const title = this.searchComicService.lastSearchedTitle;
    if (title) {
      this.searchInput.nativeElement.value = title;
      this.search(title);
    }
  }
}

import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import {Comic, Image} from '../shared/comic';
import {Subscription} from 'rxjs/Subscription';
import {SearchComicService} from '../shared/comic.service';
import {bootstrapItem} from "@angular/cli/lib/ast-tools";

@Component({
  selector: 'app-comic-detail',
  templateUrl: './comic-detail.component.html',
  styleUrls: ['./comic-detail.component.css', '../../shared/simpleGal.css'],

})
export class ComicDetailComponent implements OnInit, OnDestroy {
  constructor(
    private searchComicService: SearchComicService,
    private route: ActivatedRoute,
    private location: Location
  ) {} // каждый раз создается новый экземпляр
  private subscription: Subscription;
  previewImage: Image;
  comic: Comic;
  exists: {exists: boolean};
  busy: Promise<any>;
  ngOnInit(): void {
    this.subscription = this.route.paramMap
      .switchMap((params: ParamMap): Promise<Comic> => {
          const comicPromise = this.searchComicService.getComic(+params.get('id'));
          this.busy = comicPromise;
          return comicPromise;
        }).subscribe(comic => {
          this.comic = comic;
          this.previewImage = comic.images[0] || {path: comic.thumbnail.path, extension: comic.thumbnail.extension};
          this.searchComicService.ifComicExists(comic.title).then(exists => {
            console.log(this.exists);
            this.exists = exists;
            console.log(this.exists);
          });
      });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
  goBack(): void {
    this.location.back();
  }
  add(comic): void {
    this.busy = this.searchComicService.add(comic).then(() => {
      this.goBack();
    });
  }
}

import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

// Imports for loading & configuring the in-memory web api


import {HttpModule} from '@angular/http';

import {MomentModule} from 'angular2-moment';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {BusyModule} from 'angular2-busy';
import {ComicDetailComponent} from './comic-detail/comic-detail.component';
import {ComicsSearchComponent} from './comic-search/comic-search.component';
import {SearchComicService} from './shared/comic.service';
import {SearchRoutingModule} from './search-routing.module';


@NgModule({
  imports: [
    FormsModule,
    MomentModule,
    HttpModule,
    BrowserAnimationsModule,
    BusyModule,
    SearchRoutingModule
  ],
  declarations: [
    ComicsSearchComponent,
    ComicDetailComponent
  ],
  providers: [
    SearchComicService
  ]
})
export class SearchModule {
}

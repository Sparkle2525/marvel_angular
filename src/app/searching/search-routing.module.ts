import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ComicDetailComponent} from './comic-detail/comic-detail.component';
import {ComicsSearchComponent} from './comic-search/comic-search.component';


const comicDetail: Routes = [
  { path: ':id', component: ComicDetailComponent }
];


const routes: Routes = [
  { path: 'marvel',  component: ComicsSearchComponent, children: comicDetail }
];


@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class SearchRoutingModule {}
